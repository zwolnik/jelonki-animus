# Interface:

## Sign in

```json
{
    "sign_in": {
        "physix_rate": X,
        "sending_rate": Y,
        "world": {
            "animals": [
                {
                    "id": X,
                    "type": "fox",
                    "position": {
                        "x": X,
                        "y": Y
                    },
                    "velocity": {
                        "x": X,
                        "y": Y
                    }
                },
                ...
            ],
            "trees": [
                {
                    "position": {
                        "x": X,
                        "y": Y
                    }
                },
                ...
            ],
            "grass": [
                {
                    "position": {
                        "x": X,
                        "y": Y
                    }
                },
                ...
            ],
            "water": [
                {
                    "position": {
                        "x": X,
                        "y": Y
                    }
                },
                ...
            ]
        }
    }
}
```

## Start

```
"start"
```

## Pause

```
"pause"
```


## Stop

```
"stop"
```
