//! # Resources
//!
//! This is a place for all kinds of things related to `ECS` that are not `Systems` nor `Components`.
//!
//! Basicly all of those are directly or indirectly registered to `World` structure as resources.
use std::time::{Duration, Instant};
use std::hash::{Hash, Hasher};
use std::collections::HashSet;
use serde::{Serialize, Deserialize};
use crate::systems::{ID, Kind, Position, Velocity};

/// Type used for sending serialized payloads to the end user
pub type SendFn<'a> = Box<dyn Fn(String) -> () + 'a + Send>;

/// This struct represents all data associated with particular animal entity that are
/// interchangeable with external display system / supervisor
#[derive(Debug, Serialize, Deserialize)]
pub struct AnimalData {
    pub id:       ID,
    pub kind:     Kind,
    pub position: Position,
    pub velocity: Velocity,
}

impl PartialEq for AnimalData {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for AnimalData {}

impl Hash for AnimalData {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
        self.kind.hash(state);
    }
}

/// This struct represents all terrain fields that appear in sign_in message.
///
/// Map should define consistent area without gaps, where symulation will take place
///
/// This structure should be treated as `ReadOnly`. It is filled during sign in and doesn't change
/// throughout whole symulation process
#[derive(Debug, Serialize, Deserialize, Default)]
pub struct Map {
    grass: Vec<Position>,
    trees: Vec<Position>,
    water: Vec<Position>,
}

/// Helper struct to bind `Map` and `Animals`.
///
/// As supervisor process provides `map` definition, it is unnecessary to send it during updates.
#[derive(Debug, Serialize, Deserialize, Default)]
pub struct WorldData {
    #[serde(skip_serializing)]
    pub map:     Map,
    pub animals: HashSet<AnimalData>,
}

impl PartialEq for WorldData {
    fn eq(&self, other: &Self) -> bool {
        self.animals == other.animals
    }
}

/// Resource that indicates that `Animals` data has been updated and re-serialization is needed
#[derive(Debug, Default)]
pub struct Dirty(bool);

impl From<bool> for Dirty {
    fn from(b: bool) -> Self {
        Self(b)
    }
}

impl AsRef<bool> for Dirty {
    fn as_ref(&self) -> &bool {
        &self.0
    }
}

/// Resource that defines in what intervals should Physics / Logic updates occur during symulation.
///
/// It is registered in world with value deducated based on physics_rate from `sign_in` message
#[derive(Default)]
pub struct PhysicsInterval(Duration);

impl PhysicsInterval {
    /// Calculate `Interval` value based on rate measured in `count per second`
    ///
    /// # Examples
    /// ```
    /// use std::time::Duration;
    /// use animus_lib::resources::PhysicsInterval;
    ///
    /// assert_eq!(Duration::from_millis(500), PhysicsInterval::from_rate(2).value());
    pub fn from_rate(rate: u64) -> Self {
        Self(Duration::from_millis(1000/rate))
    }

    pub fn value(&self) -> Duration {
        self.0
    }
}

/// Resource that measures time between Physics / Logic updates
pub struct PhysicsDelta(Instant);

impl Default for PhysicsDelta {
    fn default() -> Self {
        Self(Instant::now())
    }
}

impl From<Instant> for PhysicsDelta {
    fn from(i: Instant) -> Self {
        Self(i)
    }
}

impl PhysicsDelta {
    /// Returns time elapsed since initialization
    pub fn value(&self) -> Duration {
        self.0.elapsed()
    }
}

/// Resource that defines in what intervals should Updates been sent during symulation.
///
/// It is registered in world with value deducated based on update_rate from `sign_in` message
#[derive(Default)]
pub struct UpdateInterval(Duration);

impl UpdateInterval {
    /// Calculate `Interval` value based on rate measured in `count per second`
    ///
    /// # Examples
    /// ```
    /// use std::time::Duration;
    /// use animus_lib::resources::UpdateInterval;
    ///
    /// assert_eq!(Duration::from_millis(500), UpdateInterval::from_rate(2).value());
    pub fn from_rate(rate: u64) -> Self {
        Self(Duration::from_millis(1000/rate))
    }

    pub fn value(&self) -> Duration {
        self.0
    }
}

/// Resource that measures time between Update sending
pub struct UpdateDelta(Instant);

impl Default for UpdateDelta {
    fn default() -> Self {
        Self(Instant::now())
    }
}

impl From<Instant> for UpdateDelta {
    fn from(i: Instant) -> Self {
        Self(i)
    }
}

impl UpdateDelta {
    /// Returns time elapsed since initialization
    pub fn value(&self) -> Duration {
        self.0.elapsed()
    }
}

#[derive(Default)]
pub struct MaxSpeed(pub f32);
