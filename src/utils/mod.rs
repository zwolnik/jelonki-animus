//! # Utils
//!
//! This module contains all tools that are not directly related to ECS
//!
//! This is a common place for types not registered in a World structure
use std::ops;
use serde::{Serialize, Deserialize};
use rand::Rng;
use crate::resources::WorldData;

pub mod input;

/// This enum represents current state of symulation
///
/// Symulation lifecycle should be ordered exactly as those enum variants except that Running and
/// Paused may be interchanged infinitely
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum State {
    Uninitialized,
    Initialized,
    Running,
    Paused,
    Finished,
}

impl State {
    pub fn new() -> Self {
        Self::Uninitialized
    }
}

/// Data structure for easy deserializing SignInPayload with Serde
///
/// This should directly corespond with API documented on repository level
#[derive(Debug, Deserialize, Default)]
pub struct SignInData {
    pub world:       WorldData,
    pub max_speed: u64,
    pub physix_rate: u64,
    pub update_rate: u64,
}

#[derive(Clone, Copy, Serialize, Deserialize, Default, Debug, PartialEq)]
pub struct Vector2 {
    pub x: f32,
    pub y: f32,
}

impl Vector2 {
    pub fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }

    pub fn dist(&self, other: &Vector2) -> f32 {
        ((other.x - self.x).powi(2) + (other.y - self.y).powi(2)).sqrt()
    }

    pub fn random() -> Self {
        let mut rng = rand::thread_rng();
        Self { x: rng.gen_range(-0.1, 0.1), y: rng.gen_range(-0.1, 0.1) }
    }

    pub fn len(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }

    pub fn limit(&mut self, desired: f32) {
        *self = (*self / self.len()) * desired;
    }

    pub fn set_mag(self, max_speed: f32) -> Self {
        self * max_speed / self.len()
    }

    pub fn normalized(self) -> Self {
        self.set_mag(1.0)
    }
}

impl ops::Add for Vector2 {
    type Output = Vector2;

    fn add(self, rhs: Vector2) -> Self::Output {
        Self {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

impl ops::AddAssign for Vector2 {
    fn add_assign(&mut self, rhs: Vector2) {
       self.x += rhs.x;
       self.y += rhs.y;
    }
}

impl ops::Sub for Vector2 {
    type Output = Vector2;

    fn sub(self, rhs: Vector2) -> Self::Output {
        Self {
            x: self.x - rhs.x,
            y: self.y - rhs.y,
        }
    }
}

impl ops::SubAssign for Vector2 {
    fn sub_assign(&mut self, rhs: Vector2) {
       self.x -= rhs.x;
       self.y -= rhs.y;
    }
}

impl ops::Mul<f32> for Vector2 {
    type Output = Vector2;

    fn mul(self, rhs: f32) -> Self::Output {
        Self {
            x: self.x * rhs,
            y: self.y * rhs,
        }
    }
}

impl ops::MulAssign<f32> for Vector2 {
    fn mul_assign(&mut self, rhs: f32) {
       self.x *= rhs;
       self.y *= rhs;
    }
}

impl ops::Div<f32> for Vector2 {
    type Output = Vector2;

    fn div(self, rhs: f32) -> Self::Output {
        Self {
            x: self.x / rhs,
            y: self.y / rhs,
        }
    }
}

impl ops::DivAssign<f32> for Vector2 {
    fn div_assign(&mut self, rhs: f32) {
       self.x /= rhs;
       self.y /= rhs;
    }
}

impl ops::Neg for Vector2 {
    type Output = Vector2;

    fn neg(self) -> Self::Output {
        Self {
            x: -self.x,
            y: -self.y,
        }
    }
}
