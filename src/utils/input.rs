//! # InputHandler
//!
//! `Input handler` works as a poller for given input source.
//! It consists of user provided callbacks (handlers) activated by certain keys in input source.
//!
//! *It is required that input source sends messages in a form of valid `json` format.*
//! *Each message has to be handled or Input Handler will panic*

#[allow(unused)]
use log::{error, warn, info, debug, trace};

use std::io;
use std::thread::{self, JoinHandle};
use std::sync::mpsc::{self, Receiver};
use std::sync::{Arc, atomic::{AtomicBool, Ordering}};
use std::time::Duration;
use std::collections::HashMap;

/// Generic type for reading from input source
pub type InputFn = Box<dyn Fn() -> String + Send>;
/// Generic type for user provided handlers
type Handler<'a> = Box<dyn FnMut(Option<String>) -> () + 'a>;

/// Structure that provides interface to subscribe for certain json messages
///
/// # Examples
/// 
/// ```
/// use std::sync::mpsc;
/// use animus_lib::utils::input::InputHandler;
///
/// let (input_tx, input_rx) = mpsc::channel();
/// let input_reader = move || input_rx.recv().unwrap_or_default();
///
/// let (handler_tx, handler_rx) = mpsc::channel();
/// let mut handler = InputHandler::new(Box::new(input_reader));
///
/// handler.register_handler("keyword", Box::new(move |payload| {
///     handler_tx.send(payload.unwrap().to_owned()).unwrap()
/// }));
///
/// input_tx.send(r#"{"keyword": "some payload"}"#.to_string());
/// handler.handle();
///
/// assert_eq!(handler_rx.recv().unwrap(), r#""some payload""#);
/// ```
pub struct InputHandler<'a> {
    input:         Receiver<String>,
    handle:        Option<JoinHandle<()>>,
    handlers:      HashMap<&'static str, Handler<'a>>,
    quit_marker:   Arc<AtomicBool>,
    once_handlers: HashMap<&'static str, Handler<'a>>,
}

impl<'a> InputHandler<'a> {
    /// Creates new instance of InputHandler
    ///
    /// This method spawns a thread which calls `input` function each time `handle` is run
    pub fn new(input: InputFn) -> Self {
        let (tx, rx) = mpsc::channel();
        let quit_marker = Arc::new(AtomicBool::new(false));
        let quit_marker_clone = Arc::clone(&quit_marker);
        debug!("Initializing new input handler");
        let handle = thread::spawn(move || loop {
            thread::park();
            if quit_marker_clone.load(Ordering::Acquire) {
                info!("Shutting down input handler worker");
                break
            }
            let message = input();
            if !message.is_empty() {
                debug!("[Worker] - New message in the queue");
                tx.send(message)
                    .expect("Failed to send input to input handler");
            }
        });
        Self {
            quit_marker,
            input: rx,
            handle: Some(handle),
            handlers: HashMap::new(),
            once_handlers: HashMap::new(),
        }
    }

    /// Provides new instance of InputHandler that is registered to read from process's STDIN
    ///
    /// There is no guarantee which handler will be called when creating multiple InputHandler instances for STDIN
    pub fn from_stdin() -> Self {
        let input = Box::new(|| {
            let mut buffer = String::new();
            io::stdin().read_line(&mut buffer)
                .expect("Failed to retrieve data from stdin");
            buffer.trim().to_owned()
        });
        Self::new(input)
    }

    /// Register handler that will be triggered for each message in input that matches `key`
    pub fn register_handler(&mut self, key: &'static str, handler: Handler<'a>) {
        if !self.once_handlers.contains_key(key) {
            debug!("Registering new handler with trigger: {}", key);
            self.handlers.insert(key, handler);
        }
    }

    /// Register handler that will be triggered only once for message in input that matches `key`
    ///
    /// If it is expected that more messages with given key will come and each is supposed to be
    /// handled then it's suggested to use `register_handler` instead
    pub fn register_once_handler(&mut self, key: &'static str, handler: Handler<'a>) {
        if !self.once_handlers.contains_key(key) {
            debug!("Registering new one time handler with trigger: {}", key);
            self.once_handlers.insert(key, handler);
        }
    }

    /// Tries to retrieve a message from input provided and call user handler for it. Retrieving is managed in **non** polling
    /// manner.
    ///
    /// # Panics
    ///
    /// - When message is not valid json
    /// - When message is not supported json type
    ///     - Supported types are only: Strings and Objects with one field
    /// - When there is no callback registered for given message
    pub fn handle(&mut self) {
        self.handle.as_ref().unwrap().thread().unpark();
        if let Ok(input) = self.input.recv_timeout(Duration::from_millis(5)) {
            info!("Received message from input");
            let input: serde_json::Value = serde_json::from_str(input.as_str())
                .expect("Failed to parse json from input");
            let (key, payload) = match input {
                serde_json::Value::Object(map) => {
                    if map.keys().count() != 1 { 
                        panic!("Unsupported message format");
                    }
                    map.into_iter()
                        .map(|(k, v)| (k, Some(v.to_string())))
                        .next()
                        .unwrap()
                },
                serde_json::Value::String(s) => {
                    (s, None)
                },
                _ => panic!("Unsupported message format"),
            };
            debug!("Key: {}", key);
            debug!("Payload: {:?}", payload);
            if self.handlers.contains_key(key.as_str()) {
                self.handlers.get_mut(key.as_str()).unwrap()(payload);
            } else if self.once_handlers.contains_key(key.as_str()) {
                self.once_handlers.remove(key.as_str()).unwrap()(payload);
            } else {
                panic!("No handler registered for this message");
            }
        }
    }
}

impl<'a> Drop for InputHandler<'a> {
    fn drop(&mut self) {
        debug!("Shutting down input handler");
        self.quit_marker.store(true, Ordering::Release);
        self.handle.as_ref().unwrap().thread().unpark();
        self.handle
            .take()
            .unwrap()
            .join()
            .expect("Failed to join InputHandler worker");
    }
}

#[cfg(test)]
mod input_handler_tests {
    use super::*;
    use test_case::test_case;
    use std::rc::Rc;
    use std::cell::RefCell;
    use std::sync::mpsc::Sender;

    #[allow(unused)]
    fn init_handler<'a>() -> (InputHandler<'a>, Sender<String>) {
        let (tx, rx) = mpsc::channel();
        (InputHandler::new(Box::new(move || rx.recv().unwrap_or_default())), tx)
    }

    #[test]
    #[should_panic(expected = "No handler registered for this message")]
    fn should_panic_on_unregistered_messages() {
        let (mut sut, sender) = init_handler();
        sender.send(r#""0xDEADBEEF""#.to_owned()).unwrap();
        sut.handle();
    }

    #[test_case(r#"23"#                    ; "Number as a message")]
    #[test_case(r#"true"#                  ; "Boolean as a message")]
    #[test_case(r#"null"#                  ; "Null as a message")]
    #[test_case(r#"[10, 20, 30]"#          ; "Array as a message")]
    #[test_case(r#"{"o1":"v1","o2":"v2"}"# ; "Multiple objects in message")]
    #[should_panic(expected = "Unsupported message format")]
    fn should_panic_on_unsupported_message_formats(message: &str) {
        let (mut sut, sender) = init_handler();
        sender.send(message.to_owned()).unwrap();
        sut.handle();
    }

    #[test]
    fn should_pass_payload_to_handler() {
        let (mut sut, sender) = init_handler();
        let result = Rc::new(RefCell::new(String::new()));
        let result_clone = Rc::clone(&result);
        sut.register_once_handler("run", Box::new(move |payload: Option<String>| {
            result_clone.replace(payload.unwrap());
        }));
        sender.send(r#"{"run":"0xDEADBEEF"}"#.to_owned()).unwrap();
        sut.handle();
        assert_eq!(*result.borrow(), r#""0xDEADBEEF""#);
    }

    #[test]
    #[should_panic(expected = "No handler registered for this message")]
    fn should_run_once_handler_once() {
        let (mut sut, sender) = init_handler();
        sut.register_once_handler("0xDEADBEEF", Box::new(|_|{}));
        sender.send(r#""0xDEADBEEF""#.to_owned()).unwrap();
        sender.send(r#""0xDEADBEEF""#.to_owned()).unwrap();
        sut.handle();
        sut.handle();
    }

    #[test]
    #[should_panic(expected = "No handler registered for this message")]
    fn should_not_shadow_callbacks_when_registering_with_different_handler_types() {
        let (mut sut, sender) = init_handler();
        sut.register_once_handler("0xDEADBEEF", Box::new(|_|{}));
        sut.register_handler("0xDEADBEEF", Box::new(|_|{}));
        sender.send(r#""0xDEADBEEF""#.to_owned()).unwrap();
        sender.send(r#""0xDEADBEEF""#.to_owned()).unwrap();
        sut.handle();
        sut.handle();
    }

    #[test_case(r#""x":"y""#  ; "No braces")]
    #[test_case(r#"{"x":"y""# ; "No ending brace")]
    #[test_case(r#"abcdef"#   ; "String without quotes")]
    #[should_panic(expected = "Failed to parse json from input")]
    fn should_not_allow_invalid_json_formats(message: &str) {
        let (mut sut, sender) = init_handler();
        sender.send(message.to_owned()).unwrap();
        sut.handle();
    }
}
