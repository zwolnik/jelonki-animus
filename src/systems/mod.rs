//! # Systems and Components
//!
//! This file should consist of `Components` which forms particular `Entities` as well as `Systems`
//! that are responsible for executing symulation logic and tasks
use std::hash::Hash;
use std::collections::HashSet;
#[allow(unused)]
use log::{info, debug, trace, warn, error};
use serde::{Serialize, Deserialize};
use specs::{Entities, Component, Read, Write, ReadStorage, WriteStorage, System, VecStorage};

use crate::resources::{PhysicsInterval, PhysicsDelta, UpdateInterval, UpdateDelta, Dirty, WorldData, AnimalData, SendFn, MaxSpeed};
use crate::utils::Vector2;

/// ID used by end user to identify entity
#[derive(Debug, Component, PartialEq, Eq, Hash, Serialize, Deserialize, Copy, Clone)]
#[storage(VecStorage)]
pub struct ID(u64);

/// Kind of the animal
#[derive(Debug, Component, PartialEq, Eq, Hash, Serialize, Deserialize, Copy, Clone)]
#[storage(VecStorage)]
pub enum Kind {
    Fox,
    Deer,
    Wolf,
    Moose,
}

impl Kind {
    fn alignment_factor(&self) -> f32 {
        match *self {
            Kind::Fox => 0.001,
            Kind::Deer => 0.001,
            Kind::Wolf => 0.001,
            Kind::Moose => 0.001,
        }
    }

    fn cohesion_factor(&self) -> f32 {
        match *self {
            Kind::Fox => 0.002,
            Kind::Deer => 0.002,
            Kind::Wolf => 0.002,
            Kind::Moose => 0.002,
        }
    }

    fn separation_factor(&self) -> f32 {
        match *self {
            Kind::Fox => 0.002,
            Kind::Deer => 0.002,
            Kind::Wolf => 0.002,
            Kind::Moose => 0.002,
        }
    }

    fn perception_radius(&self) -> f32 {
        match *self {
            Kind::Fox => 10.0,
            Kind::Deer => 10.0,
            Kind::Wolf => 10.0,
            Kind::Moose => 10.0,
        }
    }
}

/// Position of entity
#[derive(Debug, Component, PartialEq, Serialize, Deserialize, Copy, Clone)]
#[storage(VecStorage)]
pub struct Position(pub Vector2);

/// Velocity of entity
#[derive(Debug, Component, PartialEq, Serialize, Deserialize, Copy, Clone)]
#[storage(VecStorage)]
pub struct Velocity(pub Vector2);

/// This system is now a stub only. All the logic it contains besides delta calculation should be
/// reipmlemented / splitted to smaller systems
pub struct Physics;

impl Physics {
    #[allow(unused)]
    fn alignment(kind: Kind, pos: Position, vel: Velocity, others: &Vec<(&Velocity, &Kind, &Position)>) -> Vector2 {
        if others.len() > 0 {
            let length = others.len() as f32;
            let res = others
                .iter()
                .map(|(&vel, _, _)| vel.0)
                .fold(Vector2::default(), |acc, e| acc + e);

            ((res / length).normalized() - vel.0) * kind.alignment_factor()
        } else {
            Vector2::default()
        }
    }

    #[allow(unused)]
    fn cohesion(kind: Kind, pos: Position, vel: Velocity, others: &Vec<(&Velocity, &Kind, &Position)>) -> Vector2 {
        if others.len() > 0 {
            let length = others.len() as f32;
            let res = others
                .iter()
                .map(|(_, _, &pos)| pos.0)
                .fold(Vector2::default(), |acc, e| acc + e);
            ((res / length - pos.0).normalized() - vel.0) * kind.cohesion_factor()
        } else {
            Vector2::default()
        }
    }

    #[allow(unused)]
    fn separation(kind: Kind, pos: Position, vel: Velocity, others: &Vec<(&Velocity, &Kind, &Position)>) -> Vector2 {
        if others.len() > 0 {
            let length = others.len() as f32;
            let res = others
                .iter()
                .map(|(_, _, &other_pos)| {
                    let vec = pos.0 - other_pos.0;
                    vec / pos.0.dist(&other_pos.0)
                })
                .fold(Vector2::default(), |acc, e| acc + e);
            ((res / length).normalized() - vel.0) * kind.separation_factor()
        } else {
            Vector2::default()
        }
    }
}

impl<'a> System<'a> for Physics {
    type SystemData = (Entities<'a>,
                       Read<'a, PhysicsInterval>,
                       Read<'a, MaxSpeed>,
                       Write<'a, PhysicsDelta>,
                       Write<'a, Dirty>,
                       WriteStorage<'a, Velocity>,
                       ReadStorage<'a, Kind>,
                       WriteStorage<'a, Position>,);

    fn run(&mut self, (entities, interval, max_speed, mut delta, mut dirty,mut vel, kind, mut pos): Self::SystemData) {
        use specs::Join;
        if delta.value() <= interval.value() {
            return
        }
        debug!("Running `Physics` update");
        *delta = PhysicsDelta::default();
        *dirty = true.into();

        let mut accelerations = vec![];

        for (ent, &this_vel, &this_kind, &this_pos) in (&*entities, &vel, &kind, &pos).join() {
            let others = (&vel, &kind, &pos).join()
                .filter(|(_, &kind, &pos)|
                    kind == this_kind &&
                    pos != this_pos &&
                    pos.0.dist(&this_pos.0) < this_kind.perception_radius())
                .collect::<Vec<_>>();

            let alignment  = Physics::alignment(this_kind, this_pos, this_vel, &others);
            let cohesion   = Physics::cohesion(this_kind, this_pos, this_vel, &others);
            let separation = Physics::separation(this_kind, this_pos, this_vel, &others);
            accelerations.push((ent, alignment + cohesion + separation));
        }

        for (ent, acceleration) in accelerations {
            let pos = pos.get_mut(ent).unwrap();
            let vel = vel.get_mut(ent).unwrap();

            pos.0 += vel.0;
            vel.0 += acceleration;
            vel.0.limit(max_speed.0);
        }
    }
}

/// This systems creates a new `animals` list that is ready to be serialized and send
///
/// It should be fired only when the update occured and state is `Dirty`
#[derive(Default)]
pub struct Serializer;

impl<'a> System<'a> for Serializer {
    type SystemData = (Write<'a, WorldData>,
                       Write<'a, Dirty>,
                       ReadStorage<'a, ID>,
                       ReadStorage<'a, Kind>,
                       ReadStorage<'a, Position>,
                       ReadStorage<'a, Velocity>);

    fn run(&mut self, (mut data, mut dirty, id, kind, pos, vel): Self::SystemData) {
        use specs::Join;

        if !dirty.as_ref() {
            trace!("Delta under interval. Skipping serializing");
            return
        }
        debug!("Running `Serializer`");

        data.animals = HashSet::new();
        for (&id, &kind, &position, &velocity) in (&id, &kind, &pos, &vel).join() {
            data.animals.insert(AnimalData { id, kind, position: Position(position.0 + velocity.0 * 0.1), velocity });
        }
        *dirty = false.into();
    }
}

/// This system's purpose is to send updates about `Animals` state with the rate based on
/// `UpdateInterval`
///
/// This system holds a Boxed function, which is used to set output sink of the system
pub struct Sender<'a> {
    send_fn: SendFn<'a>,
}

impl<'a> Sender<'a> {
    /// Creates new system with output sink set to `send_fn`
    pub fn new(send_fn: SendFn<'a>) -> Self {
        info!("Creating sender with custom sink");
        Self { send_fn }
    }
}

/// Creates `Sender` with default sink set to process's STDOUT
impl<'a> Default for Sender<'a> {
    fn default() -> Self {
        info!("Initializing sender with external sink set to STDOUT");
        let send_fn = Box::new(|data| println!("{}", data));
        Self { send_fn }
    }
}

impl<'a, 'b> System<'a> for Sender<'b> {
    type SystemData = (Read<'a, UpdateInterval>,
                       Write<'a, UpdateDelta>,
                       Read<'a, WorldData>);

    fn run(&mut self, (interval, mut delta, data): Self::SystemData) {
        if delta.value() <= interval.value() {
            trace!("Delta under interval. Skipping sending");
            return
        }
        debug!("Sending `WorldData`");
        *delta = UpdateDelta::default();
        trace!("{:#?}", data.animals);
        self.send_fn.as_ref()(serde_json::to_string(&*data).unwrap());
    }
}

#[allow(unused)]
#[cfg(test)]
mod system_tests {
    pub use super::*;
    pub use specs::{Builder, Dispatcher, DispatcherBuilder, World, WorldExt};
    pub use std::error::Error;
    use serde_json::Value;

    macro_rules! build_dispatcher {
        ( $( $system:expr, $id:expr => $depends:tt),* ) => {{
            DispatcherBuilder::new()
            $(
                .with($system, $id, &$depends)
            )*
                .build()
        }}
    }

    mod physics {
        use super::*;

        #[test]
        fn should_pass_this_test() {
            init_logging();
        }
    }

    mod serializer {
        use super::*;

        #[test]
        fn should_serialize_only_if_state_is_dirty() {
            init_logging();
            let mut dispatcher = build_dispatcher!(Serializer, "" => []);
            let mut world = setup_world(1, 1, DATA_IN_PAYLOAD_MINIMAL);

            *world.fetch_mut::<Dirty>() = false.into();

            dispatcher.dispatch(&mut world);
            world.maintain();

            assert_eq!(
                serde_json::to_string(&*world.fetch::<WorldData>()).unwrap(),
                r#"{"animals":[]}"#
            );

            *world.fetch_mut::<Dirty>() = true.into();

            dispatcher.dispatch(&mut world);
            world.maintain();

            assert_eq!(
                *world.fetch::<WorldData>(),
                serde_json::from_str(DATA_IN_PAYLOAD_MINIMAL).unwrap()
            );
        }

        #[test]
        fn should_return_empty_list_for_no_animals() {
            init_logging();
            let mut dispatcher = build_dispatcher!(Serializer, "" => []);
            let mut world = setup_world(1, 1, DATA_IN_PAYLOAD_EMPTY);

            *world.fetch_mut::<Dirty>() = true.into();

            dispatcher.dispatch(&mut world);
            world.maintain();

            assert_eq!(
                serde_json::to_string(&*world.fetch::<WorldData>()).unwrap(),
                r#"{"animals":[]}"#
            );
        }

        #[test]
        fn should_produce_exact_set_on_unchanged_data() {
            init_logging();
            let mut dispatcher = build_dispatcher!(Serializer, "" => []);
            let mut world = setup_world(1, 1, DATA_IN_PAYLOAD_MINIMAL);

            *world.fetch_mut::<Dirty>() = true.into();

            dispatcher.dispatch(&mut world);
            world.maintain();

            assert_eq!(
                *world.fetch::<WorldData>(),
                serde_json::from_str::<WorldData>(DATA_IN_PAYLOAD_MINIMAL).unwrap()
            );
        }
    }

    mod sender {
        use super::*;
        use std::sync::{Arc, Mutex};
        use std::time::{Duration, Instant};
        use std::sync::mpsc;

        #[test]
        fn should_be_called_at_correct_rate() {
            init_logging();
            let interval = 1000u64;
            let rate = 60u64;
            let real_rate = interval/(interval/rate);

            let count = Arc::new(Mutex::new(0));
            let count_clone = Arc::clone(&count);
            let counter = Box::new(|_| { *count_clone.lock().unwrap() += 1; });

            let mut dispatcher = build_dispatcher!(Sender { send_fn: counter }, "" => []);
            let mut world = setup_world(1, rate, DATA_IN_PAYLOAD_MINIMAL);

            let timer = Instant::now();
            while timer.elapsed() < Duration::from_millis(interval) {
                dispatcher.dispatch(&mut world);
                world.maintain();
            }
            assert_eq!(*count.lock().unwrap(), real_rate);
        }

        #[test]
        fn should_output_correct_payload() -> Result<(), String> {
            init_logging();
            let (tx, rx) = mpsc::channel();
            let send_fn = Box::new(move |payload| { tx.send(payload).unwrap() });

            let mut world = setup_world(1, 1000, DATA_IN_PAYLOAD_MINIMAL);
            *world.fetch_mut::<Dirty>() = true.into();
            let mut dispatcher = build_dispatcher!(Serializer, "1" => [],
                                                   Sender { send_fn }, "" => ["1"]);
            dispatcher.dispatch(&mut world);
            world.maintain();

            let received = extract_animals_set(&rx.recv().unwrap())?;
            let expected = extract_animals_set(DATA_IN_PAYLOAD_MINIMAL)?;
            if received == expected {
                Ok(())
            } else {
                Err(format!("Failed! Received: {:?}, Desired: {:?}", received, expected))
            }
        }
    }

    fn init_logging() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    fn extract_animals_set(data: &str) -> Result<HashSet<AnimalData>, String> {
        if let Value::Object(mut map) = serde_json::from_str(data).unwrap() {
            serde_json::from_value::<HashSet<AnimalData>>(map["animals"].take())
                .map_err(|_| "Couldn't decode set payload".to_string())
        } else {
            Err("Main field is not an object".to_string())
        }
    }

    fn strip_whitespace(s: impl Into<String>) -> String {
        let mut s = s.into();
        s.retain(|c| !c.is_whitespace());
        s
    }

    #[test]
    fn strip_whitespace_should_compress_blobs() {
        let blob = "abc
            def   ghi
            j k l";
        assert_eq!(strip_whitespace(blob), "abcdefghijkl");
    }

    fn setup_world(physics_rate: u64, update_rate: u64, data: &'static str) -> World {
        let mut world = World::new();
        world.register::<ID>();
        world.register::<Kind>();
        world.register::<Position>();
        world.register::<Velocity>();

        let mut data: WorldData = serde_json::from_str(data).unwrap();
        data.animals.drain().for_each(|animal| {
            world.create_entity()
                .with(animal.id)
                .with(animal.kind)
                .with(animal.position)
                .with(animal.velocity)
                .build();
            });
        world.insert(PhysicsInterval::from_rate(physics_rate));
        world.insert(PhysicsDelta::default());
        world.insert(UpdateInterval::from_rate(update_rate));
        world.insert(UpdateDelta::default());
        world.insert(Dirty::default());
        world.insert(data);
        world
    }

    static DATA_IN_PAYLOAD_EMPTY: &'static str = r#"{"animals":[],"map":{"trees":[],"water":[],"grass":[]}}"#;
    static DATA_OUT_PAYLOAD_EMPTY: &'static str = r#"{"animals":[]}"#;
    static DATA_IN_PAYLOAD_MINIMAL: &'static str = r#"{
        "animals":[
            {"id":0,"kind":"Fox","position":{"x":2.0,"y":2.0},"velocity":{"x":0.0,"y":0.0}},
            {"id":1,"kind":"Moose","position":{"x":1.0,"y":5.0},"velocity":{"x":1.0,"y":0.0}}
        ],
        "map":{"trees":[],"water":[],"grass":[]}
    }"#;
    static DATA_OUT_PAYLOAD_MINIMAL: &'static str = r#"{
        "animals":[
            {"id":0,"kind":"Fox","position":{"x":2.0,"y":2.0},"velocity":{"x":0.0,"y":0.0}},
            {"id":1,"kind":"Moose","position":{"x":1.0,"y":5.0},"velocity":{"x":1.0,"y":0.0}}
        ]
    }"#;
}
