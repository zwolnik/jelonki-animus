#[allow(unused)]
use log::{error, warn, info, debug, trace};

use std::cell::RefCell;
use std::rc::Rc;

use specs::{Builder, DispatcherBuilder, World, WorldExt};

use animus_lib::systems::{ID, Kind, Position, Velocity, Serializer, Physics, Sender};
use animus_lib::resources::{PhysicsInterval, PhysicsDelta, UpdateInterval, UpdateDelta, Dirty, MaxSpeed};
use animus_lib::utils::{State, SignInData};
use animus_lib::utils::input::InputHandler;
use animus_lib::utils::Vector2;

fn main() {
    env_logger::init();
    let world = Rc::new(RefCell::new(World::new()));
    world.borrow_mut().register::<ID>();
    world.borrow_mut().register::<Kind>();
    world.borrow_mut().register::<Position>();
    world.borrow_mut().register::<Velocity>();

    let mut dispatcher = DispatcherBuilder::new()
        .with(Physics, "physics", &[])
        .with(Serializer, "serializer", &["physics"])
        .with(Sender::default(), "sender", &["serializer"])
        .build();

    let state = Rc::new(RefCell::new(State::new()));
    let mut input_handler = register_handlers(&state, &world);

    loop {
        use State::*;
        input_handler.handle();
        debug!("{:?}", *state.borrow());
        match *state.borrow() {
            Running => {
                let mut world = world.borrow_mut();
                dispatcher.dispatch(&mut world);
                world.maintain();
            },
            Finished => break,
            Uninitialized | Initialized | Paused => continue,
        }
    }
}

/// Helper function that contains all handlers registered to InputHandler
///
/// It contains a macro that makes clones from given Rc's, and binds them to actual handler
/// functions in a way that resulting Closure receives payload as the only argument
fn register_handlers<'a>(state: &Rc<RefCell<State>>, world: &Rc<RefCell<World>>) -> InputHandler<'a> {
    macro_rules! bind_rc_params {
        ( $($param:ident),* => $func:ident ) => {{
            $(
            let mut $param = Rc::clone($param);
            )*
            move |payload| {
                $func( $(&mut $param),* , payload)
            }
        }}
    }

    let mut ih = InputHandler::from_stdin();

    let sign_in_handler = bind_rc_params!(state, world => sign_in_handler);
    ih.register_once_handler("sign_in", Box::new(sign_in_handler));

    let start_handler = bind_rc_params!(state => start_handler);
    ih.register_handler("start", Box::new(start_handler));

    let pause_handler = bind_rc_params!(state => pause_handler);
    ih.register_handler("pause", Box::new(pause_handler));

    let stop_handler = bind_rc_params!(state => stop_handler);
    ih.register_once_handler("stop", Box::new(stop_handler));

    ih
}

/// Handler function for `sign_in` message
///
/// # Panics
///
/// This handler panics! when called with state other than `State::Uninitialized`
///
/// Also it is completely valid for it to panic! when payload that should come with `sign_in` is
/// missing or corrupted
fn sign_in_handler(state: &mut Rc<RefCell<State>>, world: &mut Rc<RefCell<World>>, payload: Option<String>) {
    if *state.borrow() != State::Uninitialized {
        panic!("Called `sign_in` on already initialized State");
    }

    let mut data: SignInData = serde_json::from_str(
        payload.expect("Failed to get `sign_in` payload").as_ref()
    ).expect("Failed to deserialize `sign_in` payload");

    info!("Received SignIn");
    trace!("SignIn data: {:#?}", data);

    info!("Registering animals");
    data.world.animals.drain().for_each(|animal| {
        world.borrow_mut()
            .create_entity()
            .with(animal.id)
            .with(animal.kind)
            .with(animal.position)
            .with(Velocity(Vector2::random()))
            .build();
        });
    info!("Physics rate: {}", data.physix_rate);
    world.borrow_mut().insert(PhysicsInterval::from_rate(data.physix_rate));
    world.borrow_mut().insert(MaxSpeed(data.max_speed as f32 / data.physix_rate as f32));
    world.borrow_mut().insert(PhysicsDelta::default());
    info!("Update rate: {}", data.update_rate);
    world.borrow_mut().insert(UpdateInterval::from_rate(data.update_rate));
    world.borrow_mut().insert(UpdateDelta::default());
    world.borrow_mut().insert(Dirty::default());
    world.borrow_mut().insert(data.world);
    state.replace(State::Initialized);
}

/// Handler function for `start` message
///
/// # Panics
///
/// This handler panics if called with `State::Uninitialized`
fn start_handler(state: &mut Rc<RefCell<State>>, _: Option<String>) {
    if *state.borrow() != State::Initialized && *state.borrow() != State::Paused {
        panic!("Called `start` on Uninitialized or already started state");
    }
    info!("Starting engine");
    state.replace(State::Running);
}

/// Handler function for `pause` message
///
/// # Panics
///
/// This handler panics if called with state other than `State::Running`
fn pause_handler(state: &mut Rc<RefCell<State>>, _: Option<String>) {
    if *state.borrow() != State::Running {
        panic!("Received `pause` while state wasn't running");
    }
    info!("Pausing engine");
    state.replace(State::Paused);
}

/// Handler function for `stop` message
fn stop_handler(state: &mut Rc<RefCell<State>>, _: Option<String>) {
    info!("Stopping engine");
    state.replace(State::Finished);
}
