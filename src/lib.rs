//! # Animus-Lib
//!
//! Common library for `Animus`
//!
//! It contains whole ECS related resources as well as application utils
pub mod systems;
pub mod resources;
pub mod utils;
